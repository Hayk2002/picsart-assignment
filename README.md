# Getting Started with this Admin APP

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

### `yarn install`

Intsalls the all necessary packages.
Run this before the starting the development server!

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Application overview

The following project stands for a small admin dashboard consisting of 3 pages:
1. Home page
2. Users page
3. User details page

On first page we will see list of Posts being fetched and a small form in order to add some moreof them.

What is more, techstack used in this project is the follwoing:
1. React with TypeScript
2. Redux-toolkit for state management
3. RTK query for handling requests from API
4. Ant-Design as UI library
5. Styled components as JSS approach to styles.

## Attention!!!
Would like to mention why I didn't use `Next.js`!
As it was mentioned in the project required to use `React Router`as we know,
`Next.js` uses folder/file based routing so that is why I picked up pure React
in order to fit in into the requirements.

