import { lazy } from 'react';
import { Route, Routes } from 'react-router-dom';

const Homepage = lazy(() => import('pages/homepage'));
const UsersPage = lazy(() => import('pages/users'));
const UserDetailsPage = lazy(() => import('pages/users/DetailsPage'));

const AppRoutes = () => (
  <Routes>
    <Route path="/" element={<Homepage />} />
    <Route path="/users" element={<UsersPage />} />
    <Route path="/users/:userId/details" element={<UserDetailsPage />} />
  </Routes>
);

export default AppRoutes;
