import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { RootState } from 'app/store';
import { useGetUsersQuery } from './usersApi';
import { getUsers } from './usersSlice';
import { filterUsersData } from 'common/utils/helpers';
import CustomTable from 'common/components/CustomTable';

const Users = () => {
  const dispatch = useDispatch();
  const { data, isLoading } = useGetUsersQuery();

  useEffect(() => {
    if (data) {
      const filteredData = filterUsersData(data);
      dispatch(getUsers(filteredData));
    }
  }, [data]);

  return <CustomTable loading={isLoading} />;
};

export default Users;
