import { apiSlice } from '../app/apiSlice';

export const usersApi = apiSlice.injectEndpoints({
  endpoints: (builder) => ({
    getUsers: builder.query<void, void>({
      query: () => '/users',
    }),
  }),
});

export const { useGetUsersQuery } = usersApi;
