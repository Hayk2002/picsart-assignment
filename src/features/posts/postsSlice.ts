import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';

import { PostType } from 'common/types';

interface IPostsState {
  posts: PostType[];
}

const initialState: IPostsState = {
  posts: [],
};

const postsSlice = createSlice({
  name: 'postsSlice',
  initialState,
  reducers: {
    getPosts: (state, action: PayloadAction<PostType[]>) => {
      state.posts = action.payload;
    },
    addPost: (state, action: PayloadAction<PostType>) => {
      state.posts.push(action.payload);
    },
    deletePost: (state, action: PayloadAction<number>) => {
      state.posts = state.posts.filter((post) => post.id !== action.payload);
    },
  },
});

export const { getPosts, addPost, deletePost } = postsSlice.actions;
export default postsSlice.reducer;
