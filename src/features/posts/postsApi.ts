import { apiSlice } from '../app/apiSlice';

export const postsApi = apiSlice.injectEndpoints({
  endpoints: (builder) => ({
    getPosts: builder.query<void, void>({
      query: () => '/posts?_limit=5',
    }),
  }),
});

export const { useGetPostsQuery } = postsApi;
