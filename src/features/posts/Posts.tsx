import { DeleteOutlined } from '@ant-design/icons';
import { Avatar, List } from 'antd';
import usePosts from 'common/hooks/usePosts';
import { PostType } from 'common/types';
import { useDispatch } from 'react-redux';
import { deletePost } from './postsSlice';

const Posts = () => {
  const dispatch = useDispatch();
  const { posts, isLoading } = usePosts();

  const handlePostDelete = (id: number) => {
    dispatch(deletePost(id));
  };

  return (
    <List
      loading={isLoading}
      itemLayout="horizontal"
      dataSource={posts}
      renderItem={(item: PostType, index) => (
        <List.Item actions={[<DeleteOutlined onClick={() => handlePostDelete(item.id)} />]}>
          <List.Item.Meta
            avatar={
              <Avatar src={`https://xsgames.co/randomusers/avatar.php?g=pixel&key=${index}`} />
            }
            title={<a href="https://ant.design">{item.title}</a>}
            description={item.body}
          />
        </List.Item>
      )}
    />
  );
};

export default Posts;
