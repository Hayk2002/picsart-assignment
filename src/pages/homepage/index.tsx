import PostCreateForm from 'common/components/PostCreateForm';
import Posts from 'features/posts/Posts';
import { HomepageWrapper } from './styled';

const Homepage = () => {
  return (
    <HomepageWrapper>
      <PostCreateForm />
      <Posts />
    </HomepageWrapper>
  );
};

export default Homepage;
