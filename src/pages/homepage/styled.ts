import { styled } from 'styled-components';

export const HomepageWrapper = styled.div`
  display: grid;
  grid-template-columns: auto 2fr;
  gap: 30px;
`;
