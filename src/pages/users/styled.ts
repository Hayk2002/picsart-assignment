import { styled } from 'styled-components';

export const DetailPageWrapper = styled.div`
  display: grid;
  grid-template-columns: auto 1fr;
`;

export const UserDetailsBlock = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 10px;

  h3 {
    margin: 0 0 5px 0;
    font-weight: bold;
  }

  p {
    margin: 0;
  }
`;
