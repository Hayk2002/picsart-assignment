import { RollbackOutlined } from '@ant-design/icons';
import { Button, Card, Skeleton } from 'antd';
import { useNavigate, useParams } from 'react-router-dom';

import useUserDetais from 'common/hooks/useUserDetails';
import { useDelay } from 'common/hooks';
import { UserDetailsBlock, DetailPageWrapper } from './styled';

const DetailsPage = () => {
  const navigate = useNavigate();
  const { userId } = useParams();
  const { userDetails } = useUserDetais(Number(userId));
  const { isLoading } = useDelay(2000);

  const goBack = () => {
    navigate('/users');
  };

  return (
    <DetailPageWrapper>
      <Button onClick={goBack}>
        <RollbackOutlined />
      </Button>
      <Card
        style={{ maxWidth: 250, margin: '0 auto' }}
        cover={
          <img
            alt="example"
            src="https://images.unsplash.com/photo-1633332755192-727a05c4013d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2960&q=80"
          />
        }
      >
        <Skeleton loading={isLoading} avatar active>
          <UserDetailsBlock>
            <h3>Full name</h3>
            <p>{userDetails?.name}</p>
          </UserDetailsBlock>
          <UserDetailsBlock>
            <h3>Email</h3>
            <p>{userDetails?.email}</p>
          </UserDetailsBlock>
          <UserDetailsBlock>
            <h3>Address</h3>
            <p>{userDetails?.address}</p>
          </UserDetailsBlock>
          <UserDetailsBlock>
            <h3>Age</h3>
            <p>{userDetails?.age}</p>
          </UserDetailsBlock>
        </Skeleton>
      </Card>
    </DetailPageWrapper>
  );
};

export default DetailsPage;
