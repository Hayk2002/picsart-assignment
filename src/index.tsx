import React, { Suspense } from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';
import { ApiProvider } from '@reduxjs/toolkit/query/react';
import { BrowserRouter as Router } from 'react-router-dom';

import './index.css';
import App from './app/App';
import { store } from './app/store';
import { apiSlice } from './features/app/apiSlice';
import CircleLoader from 'common/components/CircleLoader';

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement);
root.render(
  <React.StrictMode>
    <ApiProvider api={apiSlice}>
      <Provider store={store}>
        <Suspense fallback={<CircleLoader />}>
          <Router>
            <App />
          </Router>
        </Suspense>
      </Provider>
    </ApiProvider>
  </React.StrictMode>
);
