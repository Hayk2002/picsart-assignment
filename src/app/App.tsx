import { useState } from 'react';
import { HomeOutlined, UserOutlined } from '@ant-design/icons';
import { ConfigProvider, Layout, Menu } from 'antd';
import AppRoutes from 'routes';
import { NavLink, useLocation } from 'react-router-dom';
import { MainContent } from 'common/components/styled';
import Header from 'common/layouts/Header';
import { useSelector } from 'react-redux';
import { RootState } from './store';

const { Sider } = Layout;

const ligthTheme = {
  colorPrimary: '#1677ff',
  colorTextBase: '#212121',
  colorBgBase: '#ffffff',
  headerBgBase: '#ffffff',
  colorIcon: '#212121',
  colorBgContainer: '#ffffff',
  controlHeight: 42,
};

const darkTheme = {
  colorPrimary: '#c209c1',
  colorTextBase: '#ffffff',
  colorBgBase: '#000000',
  colorLink: '#c209c1',
  colorTextPlaceholder: '#ffffff',
  colorBgContainer: '#212121',
  colorIcon: '#ffffff',
  controlHeight: 42,
};

const App = () => {
  const { pathname } = useLocation();
  const [collapsed, setCollapsed] = useState(false);
  const isDarkModeActive = useSelector((state: RootState) => state.app.darkMode);

  return (
    <ConfigProvider
      theme={{
        token: isDarkModeActive ? darkTheme : ligthTheme,
      }}
    >
      <Layout style={{ height: '100%' }}>
        <Sider trigger={null} collapsible collapsed={collapsed}>
          <div className="demo-logo-vertical" />
          <Menu
            theme="dark"
            mode="inline"
            defaultSelectedKeys={[pathname]}
            items={[
              {
                key: '/',
                icon: <HomeOutlined />,
                label: <NavLink to="/">Home</NavLink>,
              },
              {
                key: '/users',
                icon: <UserOutlined />,
                label: <NavLink to="/users">Users</NavLink>,
              },
            ]}
          />
        </Sider>
        <Layout>
          <Header collapsed={collapsed} setCollapsed={setCollapsed} />
          <MainContent>
            <AppRoutes />
          </MainContent>
        </Layout>
      </Layout>
    </ConfigProvider>
  );
};

export default App;
