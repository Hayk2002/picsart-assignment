import { configureStore } from '@reduxjs/toolkit';
import { apiSlice } from 'features/app/apiSlice';
import appSlice from 'features/app/appSlice';
import postsSlice from 'features/posts/postsSlice';
import usersSlice from 'features/users/usersSlice';

export const store = configureStore({
  reducer: {
    [apiSlice.reducerPath]: apiSlice.reducer,
    app: appSlice,
    users: usersSlice,
    posts: postsSlice,
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(apiSlice.middleware),
});

/**
 * the purpose of exported types at the bottom is to utilize strong typing
 */
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
