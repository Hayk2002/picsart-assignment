import { Button, theme } from 'antd';
import { StyledHeader, StyledSwitch, ThemeControlBlock, ThemeModeText } from './styled';
import { MenuFoldOutlined, MenuUnfoldOutlined } from '@ant-design/icons';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'app/store';
import { toggleThemeMode } from 'features/app/appSlice';
import { ThemeOptions } from 'common/utils/enums';

interface IHeaderProps {
  collapsed: boolean;
  setCollapsed: (value: boolean) => void;
}

const Header = ({ collapsed, setCollapsed }: IHeaderProps) => {
  const dispatch = useDispatch();
  const isDarkModeActive = useSelector((state: RootState) => state.app.darkMode);
  const { token } = theme.useToken();

  const toggleSwitch = () => dispatch(toggleThemeMode());

  return (
    <StyledHeader style={{ background: `${token.colorBgContainer}` }} data-testid="header">
      <Button
        type="text"
        icon={collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
        onClick={() => setCollapsed(!collapsed)}
        style={{
          width: 64,
          height: 64,
          margin: '0 24px',
        }}
      />
      <ThemeControlBlock>
        <ThemeModeText>{isDarkModeActive ? ThemeOptions.Dark : ThemeOptions.Light}</ThemeModeText>
        <StyledSwitch onChange={toggleSwitch} />
      </ThemeControlBlock>
    </StyledHeader>
  );
};

export default Header;
