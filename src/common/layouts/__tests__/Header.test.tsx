import { render, screen } from '@testing-library/react';
import Header from '../Header';

jest.mock('react-redux');

it('should render Header component', () => {
  render(<Header collapsed={false} setCollapsed={() => {}} />);
  const element = screen.getAllByTestId('header');
  expect(element).toMatchSnapshot();
});
