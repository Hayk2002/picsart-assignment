import { styled } from 'styled-components';
import { Layout, Switch } from 'antd';

export const StyledHeader = styled(Layout.Header)`
  display: grid;
  grid-template-columns: 1fr auto;
  align-items: center;
  padding: 0;
`;

export const StyledSwitch = styled(Switch)`
  margin: 0 24px;
`;

export const ThemeControlBlock = styled.div`
  display: grid;
  grid-template-columns: auto auto;
  align-items: center;
`;

export const ThemeModeText = styled.p`
  margin: 0;
`;
