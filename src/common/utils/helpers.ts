interface User {
  address: {
    street: string;
  };
  company: object;
  email: string;
  id: number;
  name: string;
  phone: string;
  username: string;
  website: string;
}

export const filterUsersData = (data: User[]) => {
  return data.map((user: User) => ({
    name: user.name,
    email: user.email,
    age: user.id * 5,
    id: user.id,
    key: user.id,
    address: user.address.street,
  }));
};
