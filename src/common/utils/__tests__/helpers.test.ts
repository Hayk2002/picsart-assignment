import { filterUsersData } from '../helpers';
import * as process from 'process';

const result = [
  {
    name: 'John',
    email: 'john@gmail.com',
    age: 5,
    id: 1,
    key: 1,
    address: 'john st.',
  },
];
describe('filtered users data test suite', () => {
  beforeEach(() => {
    jest.resetModules();
    process.env.REACT_APP_BASE_URL = 'https://jsonplaceholder.typicode.com';
  });

  describe('fetched users test suite', () => {
    it('should return all users from api', async () => {
      global.fetch = jest.fn(() => Promise.resolve(result));

      const response = await fetch(process.env.REACT_APP_BASE_URL);

      expect(response).toBe(result);
    });
  });

  it('should return all users in formatted pattern', async () => {
    expect(
      filterUsersData([
        {
          name: 'John',
          id: 1,
          email: 'john@gmail.com',
          address: { street: 'john st.' },
          company: {},
          phone: '123',
          username: 'john123',
          website: 'john.com',
        },
      ])
    ).toEqual(result);
  });
});
