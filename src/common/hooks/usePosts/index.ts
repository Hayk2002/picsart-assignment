import { RootState } from 'app/store';
import { useGetPostsQuery } from 'features/posts/postsApi';
import { getPosts } from 'features/posts/postsSlice';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

const usePosts = () => {
  const dispatch = useDispatch();
  const posts = useSelector((state: RootState) => state.posts.posts);
  const { data, isLoading } = useGetPostsQuery();

  useEffect(() => {
    if (data) {
      dispatch(getPosts(data));
    }
  }, [data]);

  return { posts, isLoading };
};

export default usePosts;
