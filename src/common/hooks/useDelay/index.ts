import { useEffect, useState } from 'react';

const useDelay = (ms: number) => {
  const [isLoading, setIsLoading] = useState<boolean>(true);

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(!isLoading);
    }, ms);
  }, []);

  return { isLoading };
};

export default useDelay;
