import { RootState } from 'app/store';
import { UserType } from 'common/types';
import { useCallback, useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

const useUserDetails = (userId: number) => {
  const users = useSelector((state: RootState) => state.users.users);
  const [userDetails, setUserDetails] = useState<UserType>();

  const getUserDetails = useCallback(() => {
    const user = users.find((user: UserType) => user.id === userId);

    if (user) {
      return {
        id: user.id,
        name: user.name,
        email: user.email,
        age: user.age,
        address: `${user.address} st.`,
      };
    }
  }, [users]);

  useEffect(() => {
    const data = getUserDetails();
    setUserDetails(data);
  }, [getUserDetails]);

  return { userDetails };
};

export default useUserDetails;
