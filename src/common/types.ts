export interface UserType {
  id: number;
  name: string;
  email: string;
  age: number;
  address: string;
}

export interface PostType {
  id: number;
  userId: number;
  title: string;
  body: string;
}
