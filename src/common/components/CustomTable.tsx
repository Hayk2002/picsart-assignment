import { useSelector } from 'react-redux';
import { Button, Space, Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';

import { UserType } from 'common/types';
import { NavLink } from 'react-router-dom';
import SearchInput from './SearchInput';
import { RootState } from 'app/store';
import { useEffect, useState } from 'react';

interface ICustomTable {
  loading: boolean;
}

const columns: ColumnsType<UserType> = [
  {
    title: 'Name',
    key: 'name',
    responsive: ['md', 'sm'],
    ellipsis: {
      showTitle: false,
    },
    sorter: (a, b) => (a.name > b.name ? 1 : 0),
    render: (_, record) => <NavLink to={`/users/${record.id}/details`}>{record.name}</NavLink>,
  },
  {
    title: 'Age',
    dataIndex: 'age',
    key: 'age',
    responsive: ['md', 'sm'],
    sorter: (a, b) => a.age - b.age,
  },
  {
    title: 'Email',
    dataIndex: 'email',
    key: 'email',
    responsive: ['md', 'sm'],
    ellipsis: {
      showTitle: false,
    },
  },
  {
    title: 'Action',
    key: 'action',
    responsive: ['md', 'sm'],
    render: () => (
      <Space size="middle">
        <Button type="primary">Invite</Button>
        <Button type="primary" danger>
          Delete
        </Button>
      </Space>
    ),
  },
];

const CustomTable = (props: ICustomTable) => {
  const users = useSelector((state: RootState) => state.users.users);
  const [value, setValue] = useState('');
  const [data, setData] = useState<UserType[]>([]);

  useEffect(() => {
    const filteredData = users.filter((entry: { name: string }) => {
      if (value === '') {
        return entry;
      } else {
        return entry.name.toLowerCase().includes(value.toLowerCase());
      }
    });

    setData(filteredData);
  }, [users, value]);

  return (
    <>
      <SearchInput value={value} setValue={setValue} />
      <Table dataSource={data} columns={columns} {...props} data-testid="custom-table" />
    </>
  );
};

export default CustomTable;
