import { Button, Form, Input, message, Space } from 'antd';
import { addPost } from 'features/posts/postsSlice';
import { useState } from 'react';
import { useDispatch } from 'react-redux';

const PostCreateForm = () => {
  const dispatch = useDispatch();

  const [form] = Form.useForm();
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');

  const onFinish = () => {
    message.success('Submit success!');
    dispatch(
      addPost({
        id: Number(Math.random()),
        userId: 1,
        title,
        body: description,
      })
    );
  };

  return (
    <Form form={form} layout="vertical" onFinish={onFinish} autoComplete="off">
      <Form.Item
        name="title"
        label="Title"
        rules={[{ required: true }, { type: 'string', min: 6 }]}
      >
        <Input placeholder="Post title" value={title} onChange={(e) => setTitle(e.target.value)} />
      </Form.Item>
      <Form.Item
        name="body"
        label="Description"
        rules={[{ required: true }, { type: 'string', min: 10 }]}
      >
        <Input
          placeholder="Post description"
          value={description}
          onChange={(e) => setDescription(e.target.value)}
        />
      </Form.Item>
      <Form.Item>
        <Space>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Space>
      </Form.Item>
    </Form>
  );
};

export default PostCreateForm;
