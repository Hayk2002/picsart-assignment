import { styled } from 'styled-components';
import { Layout, Input } from 'antd';

export const UsersPageTitle = styled.h1`
  color: navy;
  font-size: 24px;
`;

export const MainContent = styled(Layout.Content)`
  margin: 24px;
  min-height: 280px;
  overflow: auto;
`;

export const StyledInput = styled(Input)`
  margin: 0 0 20px;
`;
