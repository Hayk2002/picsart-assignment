import { StyledInput } from './styled';

interface ISearchInputProps {
  value: string;
  setValue: (value: string) => void;
}

interface SyntheticEvent<T> {
  target: EventTarget & T;
}

const SearchInput = ({ value, setValue }: ISearchInputProps) => {
  const onChnage = (e: SyntheticEvent<{ value: string }>) => {
    const currValue = e.target.value;
    setValue(currValue);
  };

  return (
    <StyledInput
      placeholder="Search Name"
      value={value}
      onChange={onChnage}
      data-testid="search-input"
    />
  );
};

export default SearchInput;
