import { render, screen } from '@testing-library/react';
import SearchInput from '../SearchInput';

it('should render Form component for posts creation', () => {
  render(<SearchInput value={'asd'} setValue={() => {}} />);
  const element = screen.getAllByTestId('search-input');
  expect(element).toMatchSnapshot();
});
